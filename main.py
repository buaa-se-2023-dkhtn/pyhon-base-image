from flask import Flask,make_response,request,jsonify
try:
    from handler import handler
except:
    from alternative_handler import handler
import json

app = Flask(__name__)

@app.route('/',methods=['POST','GET'])
def hello():
    try:
        req = {}
        resp_data = handler(req)

        if isinstance(resp_data,str):
            return resp_data,200
        else:
            return jsonify(resp_data),200
    except Exception as e:
        return jsonify({
            "ErrorMessage":"调用出错，错误原因为:" + str(e)
        }),500
        
if __name__=="__main__":
    app.run("0.0.0.0",8080)


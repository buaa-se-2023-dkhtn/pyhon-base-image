import os
import json
import subprocess

import pika
import time
import threading
from datetime import datetime

sacn_interval = os.getenv("scan_interval", 30)


class LogParserThread(threading.Thread):
    path = os.getenv("log_path")
    rabbitmq_host = os.getenv("rabbitmq_host")
    popen = None

    def __init__(self):
        super().__init__()

    @staticmethod
    def parse_log(log: str) -> dict:
        """
        Log forma should be like '2023-04-15 16:40:48 | 52101314 | INFO 	- This is a info',
        specifically '${timestamp} | ${function_id} | ${level} - ${message}'
        :param log:
        :return:
        """
        log_parts = log.split(" | ")
        timestamp, log_name = log_parts[0].strip(), log_parts[1].strip()
        log_parts = log_parts[2].split("-")
        level = log_parts[0].strip()
        message = "-".join(log_parts[1:])
        message = message.strip()

        timestamp = datetime.strptime(timestamp, "%Y-%m-%d %H:%M:%S")
        return {
            "timestamp": timestamp,
            "function_id": log_name,
            "level": level,
            "message": message
        }

    @staticmethod
    def _get_popen() -> subprocess.Popen:
        return subprocess.Popen(f"tail -f {LogParserThread.path} --pid={os.getpid()}", stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE,
                                shell=True)

    def run(self):
        if LogParserThread.popen is None:
            LogParserThread.popen = LogParserThread._get_popen()
            print(f"Popen.pid: {LogParserThread.popen.pid}")
        while True:
            try:
                logs = [LogParserThread.parse_log(log.strip().decode("utf-8")) for log in
                        LogParserThread.popen.stdout.readlines()]
                connection = pika.BlockingConnection(pika.ConnectionParameters(self.rabbitmq_host))
                channel = connection.channel()

                channel.queue_declare(queue="log_queue")
                for log in logs:
                    channel.basic_publish(exchange="", routing_key="log_queue",
                                          body=json.dumps(log).encode("utf-8"))
                channel.close()
                time.sleep(sacn_interval)
            except Exception as e:
                if LogParserThread.popen.poll() is not None:
                    LogParserThread.popen = LogParserThread._get_popen()
                else:
                    e.__str__()
                    break


if __name__ == "__main__":
    if not os.getenv("log_path"):
        raise NotImplementedError("Please set log path")
    if not os.getenv("rabbitmq_host"):
        raise NotImplementedError("Please set rabbitmq host")
    thread = LogParserThread()
    thread.daemon = True
    thread.start()
    while True:
        if not thread.is_alive():
            thread = LogParserThread()
            thread.daemon = True
            thread.start()
        time.sleep(300)

FROM python:3.9
COPY . /workspace
RUN pip install -i https://pypi.tuna.tsinghua.edu.cn/simple flask
CMD cd /workspace && ./bootstrap.sh
import logging

if __name__ == "__main__":
    logging.basicConfig(
        filename="test.log",  # path
        filemode="w",
        format='%(asctime)s | %(name)s | %(levelname)s - %(message)s',
        level=logging.INFO
    )
    logger = logging.getLogger("${function id}")  # 设置为function_id, 在日志里为%(name)s
    logger.info("This is a info")
    logger.debug("This is a debug")
    logger.error("This is a error")
    logger.warning("This is a warning")
